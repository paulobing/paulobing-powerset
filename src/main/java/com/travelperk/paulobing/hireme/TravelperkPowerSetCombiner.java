package com.travelperk.paulobing.hireme;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TravelperkPowerSetCombiner {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        int k = 2;
        List<List<Integer>> buildCombinationsSubSets = new TravelperkPowerSetCombiner().subsets(list, k);
        System.out.println(buildCombinationsSubSets);
    }

    public List<List<Integer>> subsets(List<Integer> listNumbers, int k) {
        if (k == 0) {
            List<List<Integer>> listWithEmptyList = new ArrayList<>();
            listWithEmptyList.add(new ArrayList<>());
            return listWithEmptyList;
        }
        List<List<Integer>> result = new ArrayList<>();
        for(int i = 0; i <= listNumbers.size() - k; i++) {
            Integer number = listNumbers.get(i);
            List<Integer> listWithoutFirstElement = new ArrayList<>(listNumbers.subList(i + 1, listNumbers.size()));
            List<List<Integer>> partialResult = subsets(listWithoutFirstElement, k - 1);
            for(List<Integer> list: partialResult) {
                list.add(0, number);
            }
            result.addAll(partialResult);
        }
        return result;
    }

}
