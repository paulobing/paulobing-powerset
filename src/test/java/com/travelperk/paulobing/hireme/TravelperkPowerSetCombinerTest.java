package com.travelperk.paulobing.hireme;

import static org.junit.Assert.*;
import static java.util.Arrays.asList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TravelperkPowerSetCombinerTest {
    private TravelperkPowerSetCombiner travelperkPowerSetCombiner;
    private static final List<Integer> listSize4 = Arrays.asList(1, 2, 3, 4);
    private static final List<Integer> listSize5 = Arrays.asList(1, 2, 3, 4, 5);
    
    @Before
    public void init() {
        travelperkPowerSetCombiner = new TravelperkPowerSetCombiner();
    }
    
    @Test
    public void testSubSetResult_size4Level0() {
        int k = 0;
        List<List<Integer>> buildCombinationsSubSets = travelperkPowerSetCombiner.subsets(listSize4, k);
        assertEquals(1, buildCombinationsSubSets.size());
        assertEquals(asList(Collections.emptyList()), buildCombinationsSubSets);
    }

    @Test
    public void testSubSetResult_size4Level1() {
        int k = 1;
        List<List<Integer>> buildCombinationsSubSets = travelperkPowerSetCombiner.subsets(listSize4, k);
        assertEquals(4, buildCombinationsSubSets.size());
        assertEquals(asList(asList(1), asList(2), asList(3), asList(4)), buildCombinationsSubSets);
    }

    @Test
    public void testSubSetResult_size4Level2() {
        int k = 2;
        List<List<Integer>> buildCombinationsSubSets = travelperkPowerSetCombiner.subsets(listSize4, k);
        assertEquals(6, buildCombinationsSubSets.size());
        assertEquals(asList(
                asList(1, 2),
                asList(1, 3),
                asList(1, 4),
                asList(2, 3),
                asList(2, 4),
                asList(3, 4)
        ), buildCombinationsSubSets);
    }
    
    @Test
    public void testSubSetResult_size4Level3() {
        int k = 3;
        List<List<Integer>> buildCombinationsSubSets = travelperkPowerSetCombiner.subsets(listSize4, k);
        assertEquals(4, buildCombinationsSubSets.size());
        assertEquals(asList(
                asList(1, 2, 3),
                asList(1, 2, 4),
                asList(1, 3, 4),
                asList(2, 3, 4)
        ), buildCombinationsSubSets);
    }

    @Test
    public void testSubSetResult_size4Level4() {
        int k = 4;
        List<List<Integer>> buildCombinationsSubSets = travelperkPowerSetCombiner.subsets(listSize4, k);
        assertEquals(1, buildCombinationsSubSets.size());
        assertEquals(asList(
                asList(1, 2, 3, 4)
        ), buildCombinationsSubSets);
    }

    @Test
    public void testSubSetResult_size5Level0() {
        int k = 0;
        List<List<Integer>> buildCombinationsSubSets = travelperkPowerSetCombiner.subsets(listSize5, k);
        assertEquals(1, buildCombinationsSubSets.size());
        assertEquals(asList(Collections.emptyList()), buildCombinationsSubSets);
    }

    @Test
    public void testSubSetResult_size5Level1() {
        int k = 1;
        List<List<Integer>> buildCombinationsSubSets = travelperkPowerSetCombiner.subsets(listSize5, k);
        assertEquals(5, buildCombinationsSubSets.size());
        assertEquals(asList(asList(1), asList(2), asList(3), asList(4), asList(5)), buildCombinationsSubSets);
    }

    @Test
    public void testSubSetResult_size5Level2() {
        int k = 2;
        List<List<Integer>> buildCombinationsSubSets = travelperkPowerSetCombiner.subsets(listSize5, k);
        assertEquals(10, buildCombinationsSubSets.size());
        assertEquals(asList(
                asList(1, 2),
                asList(1, 3),
                asList(1, 4),
                asList(1, 5),
                asList(2, 3),
                asList(2, 4),
                asList(2, 5),
                asList(3, 4),
                asList(3, 5),
                asList(4, 5)
        ), buildCombinationsSubSets);
    }

    @Test
    public void testSubSetResult_size5Level3() {
        int k = 3;
        List<List<Integer>> buildCombinationsSubSets = travelperkPowerSetCombiner.subsets(listSize5, k);
        
        assertEquals(10, buildCombinationsSubSets.size());
        assertEquals(asList(
                asList(1, 2, 3),
                asList(1, 2, 4),
                asList(1, 2, 5),
                asList(1, 3, 4),
                asList(1, 3, 5),
                asList(1, 4, 5),
                asList(2, 3, 4),
                asList(2, 3, 5),
                asList(2, 4, 5),
                asList(3, 4, 5)
        ), buildCombinationsSubSets);
    }

    @Test
    public void testSubSetResult_size5Level5() {
        int k = 5;
        List<List<Integer>> buildCombinationsSubSets = travelperkPowerSetCombiner.subsets(listSize5, k);
        assertEquals(1, buildCombinationsSubSets.size());
        assertEquals(asList(
                asList(1, 2, 3, 4, 5)
        ), buildCombinationsSubSets);
    }

}
