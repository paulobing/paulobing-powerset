# Powerset implementation

## Behavior
Input = (1,2,3,4), k = 2  
Output = { (1,2), (1,3), (1,4), (2,3), (2,4), (3,4) } 

## Instructions

Execute it using maven clean install or check [source file of main java class](https://bitbucket.org/paulobing/paulobing-powerset/src/master/src/main/java/com/travelperk/paulobing/hireme/TravelperkPowerSetCombiner.java) and [unit testing class file](https://bitbucket.org/paulobing/paulobing-powerset/src/master/src/test/java/com/travelperk/paulobing/hireme/TravelperkPowerSetCombinerTest.java).  
Maven:
```
mvn clean install
```

## Contact
E-mail: [paulo.bing@gmail.com](paulo.bing@gmail.com)